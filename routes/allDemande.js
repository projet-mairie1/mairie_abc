var express = require('express');
var excel = require('excel4node');
var router = express.Router();
var dbcon = require('../lib/db');


router.get('/AllDemande',function(req,res,next){
    const idUsers=req.session.gardidUser;
    if(req.session.loggedin){
        dbcon.query('SELECT idDemande,refDemande,nomClient,prenomClient,contactClient,emailClient,projectLotisParcelle,arrondissementParcelle,quatierParcelle,statutdemande.libelleStatutDemande AS libStatutD,idUser,dateDemande FROM demande JOIN statutdemande ON demande.idStatutDemande = statutdemande.idStatutDemande',function(err,rows,result){
            if(err){
                req.flash('error',err);
                console.log(err);
                res.render('allDemande',{
                    data:'',
                    idUsers: idUsers
                });   
            }else{
                res.render('allDemande',{
                    data:rows,
                    idUsers: idUsers
                });
            }
        });
    }else{
        res.redirect('/authentification/auth');
    }
});	


var workbook = new excel.Workbook();

var worksheet = workbook.addWorksheet('report 1');

// Create a reusable style
var style = workbook.createStyle({
  font: {
    size: 12
  }
});

dbcon.query('SELECT refDemande,nomClient,prenomClient,contactClient,emailClient,projectLotisParcelle,arrondissementParcelle,quatierParcelle,statutdemande.libelleStatutDemande AS libStatutD,idUser,dateDemande FROM demande JOIN statutdemande ON demande.idStatutDemande = statutdemande.idStatutDemande',function(err,result){
    if(err){
      console.log('false');
        console.log(err);
    }else{
        //console.log(result);
        //console.log(result[0].refDemande);
        worksheet.cell(1,1).string('Référence').style(style);
        worksheet.cell(1,2).string('Nom').style(style);
        worksheet.cell(1,3).string('Prénom').style(style);
        worksheet.cell(1,4).string('Contact').style(style);
        worksheet.cell(1,5).string('Email').style(style);
        worksheet.cell(1,6).string('Projet Lotissement').style(style);
        worksheet.cell(1,7).string('Arrondissement').style(style);
        worksheet.cell(1,8).string('Quatier').style(style);
        worksheet.cell(1,9).string('Statut').style(style);
        worksheet.cell(1,10).string('Date').style(style);
        for(var i = 2; i<=result.length + 1; i++){
           // console.log(result[i-1].refDemande);
               // worksheet.cell(i,1).string('laaaa').style(style);
                worksheet.cell(i,1).string(result[i-2].refDemande).style(style);
                worksheet.cell(i,2).string(result[i-2].nomClient).style(style);
                worksheet.cell(i,3).string(result[i-2].prenomClient).style(style);
                worksheet.cell(i,4).number(result[i-2].contactClient).style(style);
                worksheet.cell(i,5).string(result[i-2].emailClient).style(style);
                worksheet.cell(i,6).string(result[i-2].projectLotisParcelle).style(style);
                worksheet.cell(i,7).string(result[i-2].arrondissementParcelle).style(style);
                worksheet.cell(i,8).string(result[i-2].quatierParcelle).style(style);
                worksheet.cell(i,9).string(result[i-2].libStatutD).style(style);
                worksheet.cell(i,10).date(result[i-2].dateDemande).style(style);    
              }
    }
});

router.get('/exporter', function (req, res,next) {
    workbook.write('Liste-Demandes.xlsx', res);
});

router.get('/DemandeTraiter',function(req,res,next){
  dbcon.query("SELECT refDemande,nomClient,prenomClient,contactClient,emailClient,projectLotisParcelle,arrondissementParcelle,quatierParcelle,idStatutDemande,idUser,dateDemande FROM demande WHERE idStatutDemande='3'",function(err,rows,result){
    if(err){
        console.log('error',err);
        res.render('DemandeTraiter',{data:''});   
    }else{
        res.render('DemandeTraiter',{data:rows});
    }
});
});


/*const idUsers=req.session.gardidUser;
router.post('/updateDemande/(:idUsers)/(:idDemande)',function(req,res,next){
    console.log('POST');
    let idUsers = req.params.idUsers;
    let idDemande = req.params.idDemande;
    var data={
        idUser: idUsers,
        idStatutDemande: '1'
    };
    dbcon.query('UPDATE demande SET ? WHERE idDemande = ' + idDemande, data,function(err,result){
        if(err){
            console.log(err);
        }else{
            res.redirect('/tableauDemande/(:idUsers)')
        }
    })

});*/

module.exports = router;
