var express = require('express');
var router = express.Router();
const multer = require("multer"); 
var dbcon = require('../lib/db');
var Str = require('@supercharge/strings');

router.get('/demandePapier',function(req,res,next){
    res.render('demandePapier',{
        nom: '',
        firstName: '',
        tel: '',
        email:'',
        projetLot: '',
        arrondissement: '',
        quatier: ''

    });
});


router.post('/demandePapier',function(req,res,next){
    console.log(' La POST');
    var random_WithFiftySymbols = Str.random(6); 
    let gardeIdTypeD;
    let gardeIdStatutD;

    //let email = req.body.email;
    let nom = req.body.nom;
    let firstName = req.body.firstName;
    let tel = req.body.tel;
    let email = req.body.email;
    let projetLot = req.body.projetLot;
    let arrondissement = req.body.arrondissement;
    let quatier = req.body.quatier;
    let piece = req.body.piece;
    console.log(req.body);
    console.log(nom.length);
    //email.length === 0 || password.length === 0   || email.length === 0 || projetLot.length === 0 || arrondissement.length === 0 || quatier.length === 0
    if(nom.length === 0 || firstName.length === 0 || tel.length === 0 || email.length === 0 || projetLot.length === 0 || arrondissement.length === 0 || quatier.length === 0){
            console.log('Largeur firstttt '+firstName.length);
            req.flash('error',"Please complete all fields");
            res.render('demandePapier',{
                nom: nom,
                firstName: firstName,
                tel: tel,
                email: email,
                projetLot: projetLot,
                arrondissement: arrondissement,
                quatier: quatier
        
            });
    }else{
        if(tel.length < 11){
            console.log("Tel--- "+tel.length);
            req.flash('error',"Please enter a valid phone number");
            res.render('demandePapier',{
                nom: nom,
                firstName: firstName,
                tel: tel,
                email: email,
                projetLot: projetLot,
                arrondissement: arrondissement,
                quatier: quatier
        
            });
        }else{
            console.log("Tel "+tel.length);
            dbcon.query("SELECT idTypeDemande FROM typedemande WHERE libelléType = 'Attestation de recenssement'",function(err,result){
                gardeIdTypeD=result[0].idTypeDemande;
                console.log(" La gardeIdType:   "+gardeIdTypeD);
                if(err){
                    req.flash('error',err);
                    res.render('demandePapier',{
                        nom: nom,
                        firstName: firstName,
                        tel: tel,
                        email: email,
                        projetLot: projetLot,
                        arrondissement: arrondissement,
                        quatier: quatier
                
                    });
                    console.log('1');
                }else{
                    dbcon.query("SELECT idStatutDemande FROM statutdemande WHERE libelléStatutDemande = 'En attente'",function(err,result){
                        if(err){
                            req.flash('error',err);
                            res.render('demandePapier',{
                                nom: nom,
                                firstName: firstName,
                                tel: tel,
                                email: email,
                                projetLot: projetLot,
                                arrondissement: arrondissement,
                                quatier: quatier
                        
                            });
                            console.log('2');
                        }else{
                                                        // var upload = multer({ dest: "Upload_folder_name" }) 
                            // If you do not want to use diskStorage then uncomment it 
                                
                            var storage = multer.diskStorage({ 
                                destination: function (req, file, cb) { 
                            
                                    // Uploads is the Upload_folder_name 
                                    cb(null, "public/uploads") 
                                }, 
                                filename: function (req, file, cb) { 
                                cb(null, file.fieldname + "-" + Date.now()+".jpg") 
                                } 
                            }) 
                                
                            // Define the maximum size for uploading 
                            // picture i.e. 1 MB. it is optional 
                            const maxSize = 1 * 1000 * 1000; 
                                
                            var upload = multer({  
                                storage: storage, 
                                limits: { fileSize: maxSize }, 
                                fileFilter: function (req, file, cb){ 
                                
                                    // Set the filetypes, it is optional 
                                    var filetypes = /jpeg|jpg|png/; 
                                    var mimetype = filetypes.test(file.mimetype); 
                            
                                    var extname = filetypes.test(path.extname( 
                                                file.originalname).toLowerCase()); 
                                    
                                    if (mimetype && extname) { 
                                        return cb(null, true); 
                                    } 
                                
                                    cb("Error: File upload only supports the "
                                            + "following filetypes - " + filetypes); 
                                }  
                            
                            }).single("mypic");
                            upload(req,res,function(err) { 
  
                                if(err) { 
                          
                                    // ERROR occured (here it can be occured due 
                                    // to uploading image of size greater than 
                                    // 1MB or uploading different file type) 
                                    res.send(err) 
                                } 
                                else { 
                          
                                    // SUCCESS, image successfully uploaded 
                                    res.send("Success, Image uploaded!") 
                                } 
                            }) ;
                            gardeIdStatutD=result[0].idStatutDemande;
                           // console.log("gardeIdTypeD:   "+gardeIdTypeD);
                            //console.log("gardeIdStatutD:   "+gardeIdStatutD);
                            var data = {
                                refDemande: random_WithFiftySymbols,
                                nomClient: nom,
                                prenomClient: firstName,
                                contactClient: tel,
                                emailClient: email,
                                idTypeDemande: gardeIdTypeD,
                                projectLotisParcelle: projetLot,
                                arrondissementParcelle: arrondissement,
                                quatierParcelle	: quatier,
                                idStatutDemande: gardeIdStatutD
                            };
                           
                            dbcon.query('INSERT INTO demande SET ?', data,function(err,result){
                                if(err){
                                    req.flash('error', err);
                                    res.render('demandePapier',{
                                        nom: data.nomClient,
                                        firstName: data.prenomClient,
                                        tel: data.contactClient,
                                        email: data.emailClient,
                                        projetLot: data.projectLotisParcelle,
                                        arrondissement: data.arrondissementParcelle,
                                        quatier: data.quatierParcelle
                                    });
                                    console.log('3');
                                }else{
                                    req.flash('success', "Votre demande a été envoye avec succè!!!Et est en attente")
                                    res.render('demandePapier',{
                                        nom: '',
                                        firstName: '',
                                        tel: '',
                                        email:'',
                                        projetLot: '',
                                        arrondissement: '',
                                        quatier: ''
                            });
                        }
                    });
                        }
                    });
                }
            });


        }
    }
});


module.exports = router;
