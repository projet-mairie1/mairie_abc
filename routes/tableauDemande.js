var express = require('express');
var excel = require('excel4node');
var router = express.Router();
var dbcon = require('../lib/db');

//const idUsers=req.session.gardidUser;
router.get('/(:idUsers)/(:idDemande)',function(req,res,next){
  let idUsers = req.params.idUsers;
  let idDemande = req.params.idDemande;
  var data={
      idUser: idUsers,
      idStatutDemande: '1'
  };
  dbcon.query('UPDATE demande SET ? WHERE idDemande = ' + idDemande, data,function(err,result){
      if(err){
          console.log(err);
      }else{
          //res.redirect('/tableauDemande/(:idUsers)')
          //let idUsers = req.session.gardidUser;
          //console.log('La sassio '+ idUsers);
          var selectMesDemandes='SELECT idDemande,refDemande,nomClient,prenomClient,contactClient,emailClient,projectLotisParcelle,arrondissementParcelle,quatierParcelle,idStatutDemande,idUser,dateDemande FROM demande WHERE idUser=?';
            dbcon.query(selectMesDemandes,[idUsers],function(err,rows,result){
              console.log(result.length);
                if(err){
                    req.flash('error',err);
                    console.log(err);
                    res.render('tableauDemande',{data:''});   
                }else{
                    res.render('tableauDemande',{data:rows});
                }
            });
      }
  })
});	


router.post('/traiter/(:idUser)/(:idDemande)',function(req,res,next){
  console.log('Post Traiter');
  let idDemande = req.params.idDemande;
  console.log('La '+idDemande);
  let  idUsers = req.params. idUser;
  console.log('La '+idUsers);
  var data={
    idStatutDemande: '2'
};
dbcon.query('UPDATE demande SET ? WHERE idDemande = ' + idDemande, data,function(err,result){
    if(err){
        console.log(err);
    }else{
      var selectMesDemandes='SELECT idDemande,refDemande,nomClient,prenomClient,contactClient,emailClient,projectLotisParcelle,arrondissementParcelle,quatierParcelle,idStatutDemande,idUser,dateDemande FROM demande WHERE idUser=?';
      dbcon.query(selectMesDemandes,[idUsers],function(err,rows,result){
          if(err){
              req.flash('error',err);
              console.log(err);
              res.render('tableauDemande',{data:''});   
          }else{
              res.render('tableauDemande',{data:rows});
          }
      });
    }
  })
});

router.post('/rejeter/(:idUser)/(:idDemande)',function(req,res,next){
  console.log('Post Rejeter');
  let idDemande = req.params.idDemande;
  console.log('La '+idDemande);
  let  idUsers = req.params. idUser;
  console.log('La '+idUsers);
  var data={
    idStatutDemande: '3'
};
dbcon.query('UPDATE demande SET ? WHERE idDemande = ' + idDemande, data,function(err,result){
    if(err){
        console.log(err);
    }else{
      var selectMesDemandes='SELECT idDemande,refDemande,nomClient,prenomClient,contactClient,emailClient,projectLotisParcelle,arrondissementParcelle,quatierParcelle,idStatutDemande,idUser,dateDemande FROM demande WHERE idUser=?';
      dbcon.query(selectMesDemandes,[idUsers],function(err,rows,result){
          if(err){
              req.flash('error',err);
              console.log(err);
              res.render('tableauDemande',{data:''});   
          }else{
              res.render('tableauDemande',{data:rows});
          }
      });
    }
  })
});


module.exports = router;
