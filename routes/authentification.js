var express = require('express');
var router = express.Router();
const nodemailer = require('nodemailer');
var dbcon = require('../lib/db');

router.get('/auth',function(req,res,next){
    res.render('authentification',{
        email:'',
        password: ''

    });
});


router.post('/auth',function(req,res,next){
    let email = req.body.email;
    let password = req.body.password;
    //console.log('Vide');

    if(email.length === 0 || password.length ===0){
        req.flash('error',"Please complete all fields");
        res.render('authentification',{
            email: email,
            password: password
    
        });
    }else{
        if(password.length >= 8){
            var data={
                email: email,
                password:password
            };
            let selectUser="SELECT idUser,nomUser,password FROM user WHERE nomUser=? AND password=?";
            dbcon.query(selectUser,[email,password],function(err,result){
                if(err){
                    req.flash('error',err);
                    console.log(err);
                    res.render('authentification',{
                        email: data.email,
                        password: data.password
                    });
                }else{
                    if(result.length==0){
                        req.flash('error',"Please review the information you entered");
                        res.render('authentification',{
                            email: data.email,
                            password: data.password
                        });
                    }else if(result.length==1){
                        console.log('La 1111');
                        //req.flash('success',"Authentication completed successfully");
                        let gardidUser=result[0].idUser;
                        req.session.gardidUser=gardidUser;
                        req.session.loggedin=true;
                        req.session.email=email;
                        res.redirect('/allDemande/AllDemande');
                    }else{
                        req.flash('error',"Veuillez contacter l'administrateur système!!!");
                    }
                }
        });
        }else{
            req.flash('error', "password is mandatory and must be at least 8 characters long");
            res.render('authentification',{
                email: email,
                password: password
        
            });
        }
    }
});

router.get('/forget-password',function(req,res,next){
    res.render('forget-password',{
        email:''
    });
});
var gardnomUser;
var gardpwdUser;
router.post('/forget-password',function(req,res,next){
    console.log('POST');
    let email = req.body.email;
    if(email.length === 0){
        req.flash('error', "Please complete all fields");
        res.render('forget-password',{
            email: email
        });
    }else{
        var dataForgetPwd = {
            email: email
        };
        let selectPseudoUser="SELECT nomUser FROM user WHERE nomUser=?";
        dbcon.query(selectPseudoUser,[email],function(err,result){
            if(err){
                req.flash('error',err);
                res.render('forget-password',{
                    email: dataForgetPwd.email
                });
            }else{
                if(result.length == 0){
                    req.flash('error',"Please review the information you entered");
                    res.render('forget-password',{
                        email: dataForgetPwd.email
                    });
                }else if(result.length == 1){
                    let selectUser="SELECT nomUser,password FROM user WHERE nomUser=?";
                    dbcon.query(selectUser,[email],function(err,result){
                        if(err){
                            req.flash('error',err);
                            res.render('forget-password',{
                                email: dataForgetPwd.pseudo
                            });
                        }else{
                            if(result.length == 1){
                                gardnomUser=result[0].nomUser;
                                gardpwdUser=result[0].password;
                                let transport = nodemailer.createTransport({
                                    host: 'mail.donimail.com',
                                    port: 587,
                                    auth: {
                                       user: 'password@lapieuvretechnologique.info',
                                       pass: 'password'
                                    }
                                });
                                                                
                                const message = {
                                    from: 'password@lapieuvretechnologique.info', // Sender address
                                    to: gardnomUser,         // List of recipients
                                    subject: 'Votre mot de passe', // Subject line
                                    text: 'Votre mot de passe est: '+gardpwdUser // Plain text body
                                };
                                
                                transport.sendMail(message, function(err, info) {
                                    if (err) {
                                      console.log(err)
                                    } else {
                                      console.log(info);
                                    }
                                });
                                res.redirect('/authentification/authAfterFgPwd');
                            }else{
                                req.flash('error',"Veuillez contacter l'administrateur système!!!");
                            }
                        }
                    });
                }else{
                    req.flash('error',"Veuillez contacter l'administrateur système!!!");

                }
            }
        });
    }
});

router.get('/authAfterFgPwd',function(req,res,next){
    req.flash('success', "Please check your email to authenticate");
    res.render('authAfterFgPwd',{
        email: '',
        password: ''
    })
});

router.post('/authAfterFgPwd',function(req,res,next){
    let email = req.body.email;
    let password = req.body.password;
    //console.log('Vide');

    if(email.length === 0 || password.length ===0){
        req.flash('error',"Please complete all fields");
        res.render('authentification',{
            email: email,
            password: password
    
        });
    }else{
        if(password.length >= 8){
            var data={
                email: email,
                password:password
            };
            let selectUser="SELECT nomUser,password FROM user WHERE nomUser=? AND password=?";
            dbcon.query(selectUser,[email,password],function(err,result){
                if(err){
                    req.flash('error',err);
                    res.render('authentification',{
                        email: data.email,
                        password: data.password
                    });
                }else{
                    if(result.length==0){
                        req.flash('error',"Please review the information you entered");
                        res.render('authentification',{
                            email: data.email,
                            password: data.password
                        });
                    }else if(result.length==1){
                        console.log('La 1111');
                        //req.flash('success',"Authentication completed successfully");
                        res.redirect('/allDemande/AllDemande');
                    }else{
                        req.flash('error',"Veuillez contacter l'administrateur système!!!");
                    }
                }
        });
        }else{
            req.flash('error', "password is mandatory and must be at least 8 characters long");
            res.render('authentification',{
                email: email,
                password: password
        
            });
        }
    }
});

/*router.post('/forget-password',function(req,res,next){
    console.log('POST');
    let username = req.body.username;
    if(username.length === 0){
        req.flash('error', "Please complete all fields");
        res.render('forget-password',{
            username: username
        });
    }
});
router.post();*/

module.exports = router;
