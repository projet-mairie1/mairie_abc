-- phpMyAdmin SQL Dump
-- version 4.7.9
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1:3306
-- Généré le :  jeu. 17 sep. 2020 à 09:08
-- Version du serveur :  5.7.21
-- Version de PHP :  7.2.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `projet-mairie`
--

-- --------------------------------------------------------

--
-- Structure de la table `demande`
--

DROP TABLE IF EXISTS `demande`;
CREATE TABLE IF NOT EXISTS `demande` (
  `idDemande` int(6) NOT NULL AUTO_INCREMENT,
  `refDemande` varchar(6) NOT NULL,
  `nomClient` varchar(25) NOT NULL,
  `prenomClient` varchar(50) NOT NULL,
  `contactClient` bigint(11) NOT NULL,
  `emailClient` varchar(30) NOT NULL,
  `idTypeDemande` int(6) NOT NULL,
  `projectLotisParcelle` varchar(50) NOT NULL,
  `arrondissementParcelle` varchar(50) NOT NULL,
  `quatierParcelle` varchar(30) NOT NULL,
  `idStatutDemande` int(6) NOT NULL,
  `idUser` int(6) DEFAULT NULL,
  `dateDemande` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`idDemande`),
  KEY `idTypeDemande` (`idTypeDemande`),
  KEY `idStatutDemande` (`idStatutDemande`),
  KEY `idUser` (`idUser`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `demande`
--

INSERT INTO `demande` (`idDemande`, `refDemande`, `nomClient`, `prenomClient`, `contactClient`, `emailClient`, `idTypeDemande`, `projectLotisParcelle`, `arrondissementParcelle`, `quatierParcelle`, `idStatutDemande`, `idUser`, `dateDemande`) VALUES
(4, '8iv7yq', 'DJIMA', 'Arè', 22966699987, 'auspiciadjima@gmail.com', 1, 's', '1', 's', 2, 2, '2020-09-01 14:54:46'),
(5, 'SmdgU0', 'DJIMA', 'Arè', 22966699987, 'auspiciadjima@gmail.com', 1, 's', '1', 's', 2, 2, '2020-09-01 14:54:46'),
(6, 'Vshcjb', 'DJIMA', 'Arè', 22966699987, 'auspiciadjima@gmail.com', 1, 'gj', 's', 'gh', 2, 2, '2020-09-01 14:54:46'),
(7, 'H8W2Sv', 'DJIMA', 'Arè', 29966699987, 'auspiciadjima@gmail.com', 1, 'gj', 'dg', 'gh', 2, 2, '2020-09-01 14:54:46'),
(8, 'MEZsCi', 'DJIMA', 'Arè', 29966699987, 'auspiciadjima@gmail.com', 1, 'gj', 'dg', 'gh', 3, 2, '2020-09-01 14:54:46'),
(9, 'KohFc9', 'DJIMA', 'Arè', 22966699987, 'auspiciadjima@gmail.com', 1, 'gj', 'fff', 'gh', 3, 2, '2020-09-01 14:54:46'),
(10, 'w61cwV', 'DJIMA', 'Auspicia', 22966699987, 'auspiciadjima@gmail.com', 1, 'gj', 'fff', 'gh', 2, 2, '2020-09-01 14:54:46'),
(11, 'HcNntL', 'DJIMA', 'Arè', 22966699987, 'auspiciadjima@gmail.com', 1, 's', 'dg', 'gh', 1, 1, '2020-09-01 14:54:46'),
(12, 'rj-7EE', 'DJIMA', 'Arè', 22966699987, 'auspiciadjima@gmail.com', 1, 'gj', 'd', 'gh', 3, 2, '2020-09-01 14:54:46'),
(13, 'EPokP8', 'DJIMA', 'Arè', 22966699987, 'auspiciadjima@gmail.com', 1, 's', 'fff', 'gh', 3, 2, '2020-09-01 14:54:46'),
(14, '36-GOe', 'DJIMA', 'Arè', 22966699987, 'auspiciadjima@gmail.com', 1, 'gj', 'ff', 'gh', 3, 2, '2020-09-01 14:54:46'),
(15, 'tv6-Ko', 'AKPO', 'Zakari', 22966453822, 'auspiciadjima@gmail.com', 1, 'gj', 'ff', 'gh', 2, 2, '2020-09-01 14:54:46'),
(16, 'wb78Q6', 'DJIMA', 'Auspicia', 22966699987, 'auspiciadjima@gmail.com', 1, 'gj', 'fff', 'gh', 1, 1, '2020-09-01 14:54:46'),
(17, 'PEUOxc', 'DJIMA', 'Arè', 22966699987, 'auspiciadjima@gmail.com', 1, 'gj', 'fff', 'gh', 3, 2, '2020-09-01 14:54:46'),
(18, 'rhCsKh', 'RIRE', 'Rio', 22966699987, 'auspiciadjima@gmail.com', 1, 'gj', 'fff', 'd', 2, 2, '2020-09-01 14:54:46'),
(19, 'QoCVYL', 'DJIMA', 'Arè', 22966699987, 'auspiciadjima@gmail.com', 1, 'gj', 'fff', 'gf', 3, 2, '2020-09-01 14:54:46'),
(20, 'i5PeX5', 'DJIMA', 'Arè', 22966699987, 'auspiciadjima@gmail.com', 1, 'gj', 's', 'gh', 3, 2, '2020-09-01 14:54:46');

-- --------------------------------------------------------

--
-- Structure de la table `piece`
--

DROP TABLE IF EXISTS `piece`;
CREATE TABLE IF NOT EXISTS `piece` (
  `idPiece` int(6) NOT NULL AUTO_INCREMENT,
  `nomPièce` varchar(50) NOT NULL,
  `libellé` varchar(30) NOT NULL,
  `idDemande` int(6) NOT NULL,
  PRIMARY KEY (`idPiece`),
  KEY `idDemande` (`idDemande`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `statutdemande`
--

DROP TABLE IF EXISTS `statutdemande`;
CREATE TABLE IF NOT EXISTS `statutdemande` (
  `idStatutDemande` int(6) NOT NULL AUTO_INCREMENT,
  `libelleStatutDemande` varchar(30) NOT NULL,
  PRIMARY KEY (`idStatutDemande`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `statutdemande`
--

INSERT INTO `statutdemande` (`idStatutDemande`, `libelleStatutDemande`) VALUES
(1, 'En cours de traitement'),
(2, 'Traiter'),
(3, 'Rejeter'),
(4, 'En attente');

-- --------------------------------------------------------

--
-- Structure de la table `statutuser`
--

DROP TABLE IF EXISTS `statutuser`;
CREATE TABLE IF NOT EXISTS `statutuser` (
  `idStatutUser` int(6) NOT NULL AUTO_INCREMENT,
  `libelléStatutUser` varchar(30) NOT NULL,
  PRIMARY KEY (`idStatutUser`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `statutuser`
--

INSERT INTO `statutuser` (`idStatutUser`, `libelléStatutUser`) VALUES
(1, 'Active'),
(2, 'Désactive');

-- --------------------------------------------------------

--
-- Structure de la table `typedemande`
--

DROP TABLE IF EXISTS `typedemande`;
CREATE TABLE IF NOT EXISTS `typedemande` (
  `idTypeDemande` int(6) NOT NULL AUTO_INCREMENT,
  `libelléType` varchar(30) NOT NULL,
  PRIMARY KEY (`idTypeDemande`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `typedemande`
--

INSERT INTO `typedemande` (`idTypeDemande`, `libelléType`) VALUES
(1, 'Attestation de recenssement');

-- --------------------------------------------------------

--
-- Structure de la table `typeuser`
--

DROP TABLE IF EXISTS `typeuser`;
CREATE TABLE IF NOT EXISTS `typeuser` (
  `idTypeUser` int(6) NOT NULL AUTO_INCREMENT,
  `libelléTypeUser` varchar(30) NOT NULL,
  PRIMARY KEY (`idTypeUser`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `typeuser`
--

INSERT INTO `typeuser` (`idTypeUser`, `libelléTypeUser`) VALUES
(1, 'userSimple'),
(2, 'admin');

-- --------------------------------------------------------

--
-- Structure de la table `user`
--

DROP TABLE IF EXISTS `user`;
CREATE TABLE IF NOT EXISTS `user` (
  `idUser` int(6) NOT NULL AUTO_INCREMENT,
  `nomUser` varchar(50) NOT NULL,
  `password` varchar(30) NOT NULL,
  `pseudo` varchar(6) NOT NULL,
  `idStatutUser` int(6) NOT NULL,
  `idTypeUser` int(6) NOT NULL,
  PRIMARY KEY (`idUser`),
  KEY `user_ibfk_1` (`idTypeUser`),
  KEY `idStatutUser` (`idStatutUser`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `user`
--

INSERT INTO `user` (`idUser`, `nomUser`, `password`, `pseudo`, `idStatutUser`, `idTypeUser`) VALUES
(1, 'honorinedjima@gmail.com', 'honorine1999', 'ragou', 1, 1),
(2, 'auspiciadjima@gmail.com', 'auspicia1999', 'riz', 1, 2),
(3, 'zozodjima@gmail.com', 'zozo1999', 'yaourt', 2, 1);

--
-- Contraintes pour les tables déchargées
--

--
-- Contraintes pour la table `demande`
--
ALTER TABLE `demande`
  ADD CONSTRAINT `demande_ibfk_1` FOREIGN KEY (`idTypeDemande`) REFERENCES `typedemande` (`idTypeDemande`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `demande_ibfk_2` FOREIGN KEY (`idStatutDemande`) REFERENCES `statutdemande` (`idStatutDemande`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `demande_ibfk_3` FOREIGN KEY (`idUser`) REFERENCES `user` (`idUser`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `piece`
--
ALTER TABLE `piece`
  ADD CONSTRAINT `piece_ibfk_1` FOREIGN KEY (`idDemande`) REFERENCES `demande` (`idDemande`) ON DELETE CASCADE;

--
-- Contraintes pour la table `user`
--
ALTER TABLE `user`
  ADD CONSTRAINT `user_ibfk_1` FOREIGN KEY (`idTypeUser`) REFERENCES `typeuser` (`idTypeUser`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `user_ibfk_2` FOREIGN KEY (`idStatutUser`) REFERENCES `statutuser` (`idStatutUser`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
